package pl.aboguslawski.books;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.aboguslawski.books.model.Book;
import pl.aboguslawski.books.model.BookDTO;
import pl.aboguslawski.books.model.Person;
import pl.aboguslawski.books.service.BookService;

@Slf4j
@Controller
@RequestMapping("/book")
@AllArgsConstructor
public class BookController {

    private final BookService bookService;
    private final JavaMailSender emailSender;

    @GetMapping("/create")
    public String bookForm(Model model){
        model.addAttribute("book", new BookDTO());

        return "bookForm";
    }

    @PostMapping("/create")
    public String newBook(@RequestBody BookDTO bookDTO){
        Person author = new Person(bookDTO.getFirstName(), bookDTO.getLastName());
        Book book = new Book(bookDTO.getISBN(), bookDTO.getTitle(), author);

        bookService.addBook(book);
        sendMail(book.getTitle(), author, book.getISBN());
        return "redirect:/";
    }

    private void sendMail(String title, Person author, String isbn){

        log.info("Sending email: " + title + ", " + author + ", " + isbn);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo("adam.boguslawski1998@gmail.com");
        mailMessage.setSubject("rejestracja ksiazki");
        mailMessage.setText("Rejestracja ksiazki\n" +
                "Tytuł " + title + "\n" +
                "Numer ISBN: " + isbn + "\n" +
                "Autor: " + author + "\n" +
                "Dziekujemy za zarejestrowanie ksiazki");
        emailSender.send(mailMessage);
    }
}
