package pl.aboguslawski.books.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import pl.aboguslawski.books.model.Book;
import pl.aboguslawski.books.model.Person;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class BookService {

    private List<Book> books;

    public BookService() {
        books = new ArrayList<>();
        initDb();
    }

    public void addBook(Book book){
        books.add(book);
    }

    public Book getBook(String isbn){
        for(Book book : books){
            if (book.getISBN().equals(isbn)){
                return book;
            }
        }
        return null;
    }

    public List<Book> allBooks(){
        return books;
    }

    public boolean isISBNValid(Book book){
        return isISBNValid(book.getISBN());
    }

    public boolean isISBNValid(String ISBN) {
        ISBN = ISBN.replaceAll("-", "");
        System.out.println(ISBN);
        int sum = 0;
        for (int i = 0; i < 13; i++) {
            if (i % 2 == 0) {
                sum += Character.getNumericValue(ISBN.charAt(i));
            } else {
                sum += Character.getNumericValue(ISBN.charAt(i)) * 3;
            }
        }

        return (sum % 10) == 0;
    }

    private void initDb(){
        Person person = new Person("Jimmy", "Neutron");

        Book book1 = new Book("978-3-16-148410-0", "title1", person);
        Book book2 = new Book("978-3-16-148410-1", "title2", person);
        Book book3 = new Book("978-83-7181-510-2", "title3", person);

        books.add(book1);
        books.add(book2);
        books.add(book3);

    }


}
