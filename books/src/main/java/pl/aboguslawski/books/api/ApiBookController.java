package pl.aboguslawski.books.api;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.aboguslawski.books.model.Book;
import pl.aboguslawski.books.model.BookDTO;
import pl.aboguslawski.books.model.Person;
import pl.aboguslawski.books.service.BookService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/book")
@Slf4j
public class ApiBookController {

    private final BookService bookService;
    private final JavaMailSender emailSender;

    @GetMapping("/all")
    public List<Book> allBooks(){
        return bookService.allBooks();
    }

    @GetMapping
    public Book book(@RequestParam String isbn){
        return bookService.getBook(isbn);
    }


    @PostMapping
    public Book addBook(@RequestBody Book book){
        bookService.addBook(book);

        sendMail(book.getTitle(), book.getAuthor(), book.getISBN());
        return book;
    }

    @GetMapping("/isbn")
    public String validISBN(@RequestParam String isbn){
        if(bookService.isISBNValid(isbn)){
            return "valid isbn";
        }

        return "invalid isbn";
    }


    private void sendMail(String title, Person author, String isbn){

        log.info("Sending email: " + title + ", " + author + ", " + isbn);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo("adam.boguslawski1998@gmail.com");
        mailMessage.setSubject("rejestracja ksiazki");
        mailMessage.setText("Rejestracja ksiazki\n" +
                "Tytuł " + title + "\n" +
                "Numer ISBN: " + isbn + "\n" +
                "Autor: " + author + "\n" +
                "Dziekujemy za zarejestrowanie ksiazki");
        emailSender.send(mailMessage);
    }
}
