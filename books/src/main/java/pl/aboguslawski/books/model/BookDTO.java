package pl.aboguslawski.books.model;

import lombok.Data;

@Data
public class BookDTO {
    private String ISBN;
    private String title;
    private String firstName;
    private String lastName;
}
