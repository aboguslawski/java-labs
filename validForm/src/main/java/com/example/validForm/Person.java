package com.example.validForm;

import com.example.validForm.validator.MoneyConstraint;
import com.example.validForm.validator.PostalCodeConstraint;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be at least zero")
    private int age;

    @PostalCodeConstraint(message = "wrong postal code format")
    private String postal;


    @MoneyConstraint(message = "earning between 2000 and 3000")
    private int earning;

    @AssertTrue(message = "this must be checked")
    private Boolean isAccepting;

}
