package com.example.validForm.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostalCodeValidator implements ConstraintValidator<PostalCodeConstraint, String> {
    @Override
    public void initialize(PostalCodeConstraint contactNumber) {
    }
    @Override
    public boolean isValid(String contactField,
                           ConstraintValidatorContext cxt) {
        return contactField != null && contactField.matches("[0-9]{2}-[0-9]{3}");
    }
}
