package com.example.validForm.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MoneyValidator implements
        ConstraintValidator<MoneyConstraint, Integer> {
    @Override
    public void initialize(MoneyConstraint money) {
    }

    @Override
    public boolean isValid(Integer moneyField,
                           ConstraintValidatorContext cxt) {
        return moneyField != null && moneyField >= 2000 && moneyField <= 300;
    }


}
