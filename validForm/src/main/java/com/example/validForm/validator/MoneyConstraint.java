package com.example.validForm.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = MoneyValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MoneyConstraint {
    String message() default "Invalid money";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
