package com.example.accessingdatajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class AccessingDataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Customer("Jack", "Bauer", 5.5, new Date()));
			repository.save(new Customer("Chloe", "O'Brian", 10.0, new Date(123123123123L)));
			repository.save(new Customer("Kim", "Bauer", 123.0, new Date(1234567890L)));
			repository.save(new Customer("David", "Palmer", 0.1, new Date()));
			repository.save(new Customer("Michelle", "Dessler", 5.5, new Date(9876543210L)));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findById(1L);
			log.info("Customer found with findById(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});
			log.info("");

			// fetch by money
			log.info("Customers found with findByMoney(5.5):");
			log.info("--------------------------------------------");
			repository.findByMoney(5.5).forEach(m -> log.info(m.toString()));
			log.info("");

			// fetch by date

			log.info("Customers found with findByDate(new Date(1234567890L)):");
			log.info("--------------------------------------------");
			repository.findByDate(new Date(1234567890L)).forEach(m -> log.info(m.toString()));
			log.info("");
		};
	}

}
