package com.tje.users;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class UsersController {


    private static final List<User> users = new ArrayList<>();


    @GetMapping("/")
    public String home(Model model) throws ParseException {
        String startDateString = "20/05/2007 07:32";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date startDate = df.parse(startDateString);
        User user = new User(2, "Artur", 34, User.UserType.ADMIN, startDate);
        model.addAttribute("user", user);
        return "home";
    }

    @GetMapping("/list")
    public String list (Model model) throws ParseException {
        User u1 = new User(3, "u1" ,25, User.UserType.GUEST, new Date(System.currentTimeMillis()));
        User u2 = new User(4, "u2" ,35, User.UserType.ADMIN, new Date(System.currentTimeMillis()));
        User u3 = new User(5, "u3" ,46, User.UserType.REGISTERED, new Date(System.currentTimeMillis()));
        User u4 = new User(6, "u4" ,15, User.UserType.REGISTERED, new Date(System.currentTimeMillis()));
        User u5 = new User(7, "u5" ,27, User.UserType.REGISTERED, new Date(System.currentTimeMillis()));

        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);
        users.add(u5);

        model.addAttribute("users",users);

        return "list";
    }

    @GetMapping("/create")
    public String userForm(Model model){
        model.addAttribute("user", new User());
        return "userForm";
    }

    @PostMapping("/create")
    public String processOrder(@Valid User user, Errors errors){
        if(errors.hasErrors()){
            return "userForm";
        }
        user.setRegistrationDate(new Date(System.currentTimeMillis()));
        System.out.println("CREATED NEW USER "+user.getRegistrationDate());
        users.add(user);
        return "redirect:/list";
    }


}
