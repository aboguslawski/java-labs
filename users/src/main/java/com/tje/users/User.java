package com.tje.users;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class User {
    private int id;

    @NotNull(message = "can't be empty")
    @Size(min = 2, message = "min size 1")
    private String name;
    @NotNull(message = "can't be empty")
    @Min(value = 18, message = "must be adult")
    private int age;
    @NotNull(message = "can't be empty")
    @Size(min = 2, message = "min size 1")
    private UserType userType;

    private Date registrationDate;

    public User(int id, String name, int age, UserType userType, Date registrationDate) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.userType = userType;
        this.registrationDate = registrationDate;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String registrationDateString(){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        return format.format(registrationDate);
    }

    public long daysFromRegistration(){
        long diffInMillies = Math.abs(new Date().getTime() - registrationDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diff;


    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public static enum UserType {
        GUEST, REGISTERED, ADMIN
    }
}
