package pl.edu.ug.tent.springmvcdemo.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Book {

  @Id
  @GeneratedValue
  private long id;
  private String title;
  private int yop;

  @ManyToMany
  private List<Publisher> publishers;

  public Book(String title, int yop) {
    this.title = title;
    this.yop = yop;
  }

}
