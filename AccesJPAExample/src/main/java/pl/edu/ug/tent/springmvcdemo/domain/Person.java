package pl.edu.ug.tent.springmvcdemo.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Person {

  private String id;
  private String firstName;
  private int yob;

  public Person(String firstName, int yob) {
    this.firstName = firstName;
    this.yob = yob;
  }

}
