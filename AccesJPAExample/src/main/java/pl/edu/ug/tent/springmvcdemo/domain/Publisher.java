package pl.edu.ug.tent.springmvcdemo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Publisher {

  @Id
  @GeneratedValue
  private long id;
  private String name;
  private int yof;

  @ManyToMany(mappedBy = "publishers")
  private List<Book> books;

  public Publisher(String name, int yof) {
    this.name = name;
    this.yof = yof;
  }

}
