package pl.edu.ug.tent.springmvcdemo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue
    private long id;
    private String street;
    private String city;

    @OneToOne
    private Person2 person;

    public Address(String street, String city, Person2 person) {
        this.street = street;
        this.city = city;
        this.person = person;
    }
}
