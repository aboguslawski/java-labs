package pl.edu.ug.tent.springmvcdemo.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Car {

  @Id
  @GeneratedValue
  private long id;
  private String make;
  private int yop;

  public Car(String make, int yop) {
    this.make = make;
    this.yop = yop;
  }

}
