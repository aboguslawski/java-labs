package pl.edu.ug.tent.springmvcdemo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Person2 {

    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private int yob;

    @OneToOne
    private Address address;

    @OneToMany
    private List<Car> cars;

    public Person2(String firstName, int yob, Address address) {
        this.firstName = firstName;
        this.yob = yob;
        this.address = address;
        this.cars = new ArrayList<>();
    }
}
